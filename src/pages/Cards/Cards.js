import React, {Component} from 'react';
import axios from 'axios';
import Loader from '../../components/Loader/Loader';
import Card from "../../components/Card/Card";
import './Cards.scss';
import Modal from "../../components/Modal/Modal";
import Button from "../../components/Button/Button";
import {getItemFromLocalStorage} from '../../helpers/getItemFromLocalStorage';
import {saveToLocalStorage} from '../../helpers/saveToLocalStorage';

class Cards extends Component {
  state = {
    isLoading: true,
    isOpenModal: false,
    favorite: getItemFromLocalStorage('favorite') || [],
    cart: getItemFromLocalStorage('cart') || [],
    cards: getItemFromLocalStorage('cards') || [],
  }

  componentDidMount() {
    axios.get('/api/phones.json')
        .then((response) => {
          this.setState({cards: response.data, isLoading: false});
          saveToLocalStorage('cards', response.data);
        })
  }

  favoriteHandler = (id) => {
    const {favorite} = this.state;
    let newCardsList;
    if (favorite.includes(id)) {
      newCardsList = favorite.filter(el => el !== id);
      this.setState({favorite: newCardsList})
    } else {
      newCardsList = [...favorite, id];
      this.setState({favorite: newCardsList})
    }
    saveToLocalStorage('favorite', newCardsList);
  }

  addToCartHandler = (id) => {
    const {isOpenModal, cart} = this.state;
    if (!cart.includes(id)) {
      const cartList = [...cart, id];
      this.setState({isOpenModal: !isOpenModal, cart: cartList});
      saveToLocalStorage('cart', cartList);
    }
  }

  closeModal = (e) => {
    if (e.target.dataset.cancel) {
      const {cart} = this.state;
      cart.pop();
      saveToLocalStorage('cart', cart);
      this.setState({isOpenModal: false, cart});
    } else {
      this.setState({isOpenModal: false});
    }
  }

  render() {
    const {isLoading, isOpenModal, favorite} = this.state;
    const cards = this.state.cards.map(item => <Card key={item.id}
                                                     {...item}
                                                     favorite={favorite}
                                                     modalHandler={this.addToCartHandler}
                                                     favoriteHandler={this.favoriteHandler}
    />);

    return (
        <>
          {isLoading ? <Loader/> : <ul className={'cards-container'}>{cards}</ul>}
          {isOpenModal && <Modal modalHandler={this.closeModal}
                                 cancel={true}
                                 header={"Do you want to add it to the cart?"}
                                 actions={
                                   <>
                                     <Button backgroundColor={"#b3382c"}
                                             text={"Ok"}
                                             clickHandler={this.closeModal}
                                     />
                                     <Button backgroundColor={"#b3382c"}
                                             text={"Cancel"}
                                             clickHandler={this.closeModal}
                                             cancel={true}
                                     />
                                   </>
                                 }


          />}
        </>
    );
  }
}

export default Cards;