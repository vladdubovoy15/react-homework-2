import React, {Component} from "react";
import ErrorBoundary from "./components/ErrorBoundary/ErrorBoundary";
import Footer from "./components/Footer/Footer";
import Header from "./components/Header/Header";
import './style/style.scss';
import Cards from "./pages/Cards/Cards";
import {BrowserRouter} from "react-router-dom";

class App extends Component {

  render() {
    return (
        <BrowserRouter>
          <ErrorBoundary>
            <Header/>
            <main className={"main"}>
              <Cards/>
            </main>
            <Footer/>
          </ErrorBoundary>
        </BrowserRouter>
    );
  }
}

export default App;
