import React, {Component} from 'react';

class Footer extends Component {
  render() {
    return (
          <footer className="page-footer">
            <span className="footer-copyright">
                © 2021 Copyright Text
            </span>
          </footer>
    );
  }
}

export default Footer;