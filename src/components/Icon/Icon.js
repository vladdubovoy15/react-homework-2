import React from 'react';
import * as Icons from '../../theme';
import './Icon.scss';
import PropTypes from "prop-types";

const Icon = ({type, color, filled, className, favoriteHandler, id}) => {
  const jsx = Icons[type];

  if (!jsx) {
    return null;
  }

  return (
      <div className={`icon icon--${type} ${className}`} onClick={() => favoriteHandler(id)}>
        {jsx({color, filled})}
      </div>
  )
};

Icon.propTypes = {
  type: PropTypes.string,
  color: PropTypes.string,
  filled: PropTypes.bool,
  className: PropTypes.string,
  id: PropTypes.string,
  favoriteHandler: PropTypes.func,
}

Icon.defaultProps = {
  type: "star",
  filled: false
}

export default Icon;