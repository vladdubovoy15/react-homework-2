import React, {Component} from 'react';
import './Card.scss';
import Icon from "../Icon/Icon";
import PropTypes from 'prop-types';

class Card extends Component {

  render() {
    const {title, price, color, img, code, id, modalHandler, favoriteHandler, favorite} = this.props;

    return (
        <>
          <li className='card z-depth-2'>
            <h3 className='card__title'>{title}</h3>
            <img src={img}
                 className={"card__image"}
                 alt={title}
                 width={200}
                 height={240}/>
            <ul>
              <li>code: {code}</li>
              <li>color: {color}</li>
              <li>price: {price}</li>
            </ul>
            <button className={"card__button red waves-effect waves-light"} onClick={() => modalHandler(id)}>Add to
              cart
            </button>
            <Icon filled={favorite.includes(id)} className={"favorite"} favoriteHandler={favoriteHandler} id={id}/>
          </li>
        </>
    );
  }
}

Card.propTypes = {
  title: PropTypes.string,
  price: PropTypes.string,
  color: PropTypes.string,
  img: PropTypes.string,
  code: PropTypes.string,
  id: PropTypes.string,
  modalHandler: PropTypes.func,
  favoriteHandler: PropTypes.func,
  favorite: PropTypes.arrayOf(PropTypes.string),
}

export default Card;