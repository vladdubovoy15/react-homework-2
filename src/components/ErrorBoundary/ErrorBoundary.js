import React, {Component} from 'react';
import Error from "../../pages/Error/Error";

class ErrorBoundary extends Component {
  state = {
    errorPresent: false
  }

  static getDerivedStateFromError(error) {
    console.log(error)
    return {errorPresent: true}
  }

  render() {
    const {children} = this.props;
    const {errorPresent} = this.state;

    return errorPresent ? <Error/> : children
  }
}

export default ErrorBoundary;