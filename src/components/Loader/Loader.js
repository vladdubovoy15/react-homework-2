import React, {Component} from 'react';

class Loader extends Component {
  render() {
    return (
        <div className="progress red loader">
          <div className="indeterminate white"></div>
        </div>
    );
  }
}

export default Loader;