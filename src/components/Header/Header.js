import React, {Component} from 'react';
import {Link} from 'react-router-dom';

class Header extends Component {
  render() {
    return (
        <header>
          <nav>
            <div className="nav-wrapper">
              <Link to="/" className="brand-logo center">Mobile Store</Link>
              <ul id="nav-mobile" className="left hide-on-med-and-down">
                <li><Link to="/cart">Cart</Link></li>
                <li><Link to="/favorite">Favorite</Link></li>
              </ul>
            </div>
          </nav>
        </header>
    );
  }
}

export default Header;